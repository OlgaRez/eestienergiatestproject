﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eestienergia.Tests
{
    class LoginTests : TestBase
    {
        [Test]
        public void TestSuccesfulLogin()
        {
            var driver = Session.WebDriver.Value;
            Login(Configuration["ValidPersonalCode"], Configuration["ValidReferentialNumber"]);
            Thread.Sleep(1000);

            var actual = driver.FindElement(By.ClassName("header__button-group")).Text;
            Assert.AreNotEqual("E-teenindus", actual);
            BrowserSession.ClearBrowserCache(driver);
        }

        [Test]
        public void TestNotRegistredPersonalcode()
        {
            var driver = Session.WebDriver.Value;
            Login(Configuration["InvalidPersonalCode"], Configuration["ValidReferentialNumber"]);          

            var generalError = driver.FindElement(By.Id("error")).WaitDisplayed().Text;
            Assert.AreEqual("Sisselogimine ei õnnestunud. Palun kontrollige, kas sisestasite isikukoodi ja viitenumbri õigesti.", generalError);            
        }

        [Test]
        public void TestEmptyInput()
        {
            var driver = Session.WebDriver.Value;
            OpenLoginForm();
            FindInputByLabel(driver, "Isikukood").WaitDisplayed();
            driver.WaitElement(By.CssSelector("[class='button green full']")).Click();

            var generalError = driver.FindElement(By.Id("error")).Text;
            Assert.AreEqual("Sisselogimine ebaõnnestus.", generalError);
            Assert.AreEqual("Selle välja täitmine on kohustuslik.", FindErrorMessageByLabel(driver, "Isikukood"));
            Assert.AreEqual("Selle välja täitmine on kohustuslik. Lepingu viitenumber on 11-kohaline number.", FindErrorMessageByLabel(driver, "Viitenumber"));
        }

        //Test Fail is expected: Screenshot is done
        [Test]
        public void TestInvalidPersonalCodeFormat()
        {
            
            var driver = Session.WebDriver.Value;
            Login("test", Configuration["ValidReferentialNumber"]);            

            var generalError = driver.FindElement(By.Id("error")).WaitDisplayed().Text;
            Assert.AreEqual("Sisselogimine ei õnnestunud. Palun kontrollige, kas sisestasite isikukoodi ja viitenumbri õigesti.", generalError);
            Assert.AreEqual("Selle välja täitmine on kohustuslik. Vale isikukoodi formaat", FindErrorMessageByLabel(driver, "Isikukood"));
        }

        [Test]
        public void TestInvalidReferentialNumberFormat()
        {
            var driver = Session.WebDriver.Value;
            Login(Configuration["InvalidPersonalCode"], "test");
           
            var generalError = driver.FindElement(By.Id("error")).Text;
            Assert.AreEqual("Sisselogimine ebaõnnestus.", generalError);
            Assert.AreEqual("Selle välja täitmine on kohustuslik. Lepingu viitenumber on 11-kohaline number.", FindErrorMessageByLabel(driver, "Viitenumber"));
        }

        [Test]
        public void TestSuccesfulLogout()
        {
            var driver = Session.WebDriver.Value;
            Login(Configuration["ValidPersonalCode"], Configuration["ValidReferentialNumber"]);
            Thread.Sleep(1000);
            driver.Navigate().GoToUrl(Session.GetFullUrl("et/avaleht"));

            var selfServiceButton = driver.FindElement(By.ClassName("header__button-group"));
            Assert.AreNotEqual("E-teenindus", selfServiceButton.Text);
            selfServiceButton.Click();

            var selfServiceDropDownMenu = driver.FindElement(By.CssSelector("[class='dropdown-container dropdown--no-spacing dropdown--align-right']"));
            Assert.True(selfServiceDropDownMenu.Displayed);            
            var logoutButton = selfServiceDropDownMenu.FindElement(By.LinkText("Logi välja")).WaitDisplayed();
            logoutButton.Click();

            Assert.AreEqual("E-teenindus", driver.WaitElement(By.CssSelector("[class='e-btn btn--has-icon btn--secondary btn--sm']")).Text);
        }

        private void Login(string personalCode, string referentialNumber)
        {
            var driver = Session.WebDriver.Value;
            OpenLoginForm();
            FindInputByLabel(driver, "Isikukood").SendKeys(personalCode);
            FindInputByLabel(driver, "Viitenumber").SendKeys(referentialNumber);
            driver.FindElement(By.CssSelector("[class='button green full']")).Click();
        }

        private void OpenLoginForm()
        {
            var driver = Session.WebDriver.Value;
            driver.Navigate().GoToUrl(Session.GetFullUrl("et/avaleht"));

            driver.FindElement(By.CssSelector("[class='e-btn btn--has-icon btn--secondary btn--sm']")).Click();
            var loginModal = driver.WaitElement(By.Id("loginModalRoot"));
            driver.SwitchTo().ActiveElement();
            Assert.True(loginModal.Displayed);
            driver.FindElement(By.CssSelector("[data-tab='refnum']")).Click();
        }

        private static IWebElement FindInputByLabel(IWebDriver driver, string inputLabel)
        {
            var label = driver.WaitElement(By.XPath($"//label[contains(text(),'{inputLabel}')]"));
            var inputId = label.GetAttribute("for");
            return driver.FindElement(By.Id(inputId));
        }

        private static string FindErrorMessageByLabel(IWebDriver driver, string inputLabel)
        {
            var label = driver.WaitElement(By.XPath($"//label[contains(text(),'{inputLabel}')]"));
            var inputId = label.GetAttribute("for");
            return driver.FindElement(By.Id(inputId + "-err")).Text;
        }

    }
}
