﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eestienergia
{
    public static class WebDriverExtensions
    {
        public static IWebElement WaitElement(this IWebDriver driver, By by, int waitSeconds = 15)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSeconds));
            try
            {
                var passwordInput = wait.Until((webDriver) =>
                {
                    try
                    {
                        return webDriver.FindElement(by);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                });
                return passwordInput;
            }
            catch (Exception ex)
            {
                throw new Exception("Field not found:" + by, ex);
            }
        }

        public static IWebElement WaitElement(this IWebDriver driver, IWebElement container, By by, int waitSeconds = 15)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitSeconds));
            try
            {
                var passwordInput = wait.Until((webDriver) =>
                {
                    try
                    {
                        return container.FindElement(by);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                });
                return passwordInput;
            }
            catch (Exception ex)
            {
                throw new Exception("Field not found:" + by, ex);
            }
        }

        public static IWebElement WaitDisplayed(this IWebElement webElement, int waitSeconds = 15)
        {
            var waitTime = 0.5;
            double remaning = waitSeconds;
            while (remaning > 0)
            {
                if (webElement.Displayed)
                {
                    return webElement;
                }
                remaning -= waitTime;
                Thread.Sleep(TimeSpan.FromSeconds(waitTime));
            }
            throw new Exception("Not visible:" + webElement.ToString());
        }

        public static void SoftAlertConfirmation(this IWebDriver driver)
        {
            try
            {
                var alert = driver.SwitchTo().Alert();
                alert.Accept();
            }
            catch (NoAlertPresentException) { }
        }
        public static bool ElementIsPresent(this IWebDriver driver, By by)
        {
            //try
            //{
            //    return driver.FindElement(by).Displayed;
            //}
            //catch (NoSuchElementException)
            //{
            //    return false;
            //}
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }
}

