﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eestienergia
{
    class BrowserSession
    {
        public readonly Uri UrlBase;

        public Lazy<IWebDriver> WebDriver { get; }

        public BrowserSession(Uri baseUri)
        {
            UrlBase = baseUri;
            WebDriver = new Lazy<IWebDriver>(() =>
            {
                var options = new ChromeOptions();
                //_options.AddArgument("--window-size=1920,1080");
                options.AddArgument("--start-maximized");
                options.AddArgument("--force-device-scale-factor=1");
                options.AddArgument("--high-dpi-support=1");
                var driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), options);
                return driver;
            }, true);
        }
        public string GetFullUrl(string relativeUrl)
        {
            if (Uri.TryCreate(UrlBase, relativeUrl, out var result))
            {
                return result.AbsoluteUri;
            }
            throw new NotSupportedException("Invalid relative url");
        }

        public static void ClearBrowserCache(IWebDriver driver)
        {
            driver.Manage().Cookies.DeleteAllCookies(); //delete all cookies
            Thread.Sleep(2000); //wait 7 seconds to clear cookies.
        }
        public void Dispose()
        {
            if (WebDriver.IsValueCreated)
            {
                WebDriver.Value.Quit();
                WebDriver.Value.Dispose();
            }
        }

    }
}
