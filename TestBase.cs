﻿using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Eestienergia
{
    class TestBase

    {
        public BrowserSession Session { get; private set; }
        public IConfigurationRoot Configuration { get; private set; }

        public TestBase()
        {
            Configuration = new ConfigurationBuilder()
           .AddJsonFile("appconfig.json")
           .AddUserSecrets<TestBase>() //Local
           .Build();
        }

        [OneTimeSetUp]
        public void BeforeTests()
        {

            var baseUrl = Configuration["BaseUrl"];
            Session = new BrowserSession(new Uri(baseUrl));

        }

        [OneTimeTearDown]
        public void AfterTests()
        {
            if (TestContext.CurrentContext.Result.Outcome != ResultState.Success)
            {
                var screenshot = ((ITakesScreenshot)Session.WebDriver.Value).GetScreenshot();
                if (!Directory.Exists("Screenshots"))
                {
                    Directory.CreateDirectory("Screenshots");
                }
                var fileName = $"Screenshots\\{TestContext.CurrentContext.Test.FullName}_{DateTime.Now:yyyy-MM-dd_HH-mm-ss.fffff}.png";
                screenshot.SaveAsFile(fileName, ScreenshotImageFormat.Png);
                TestContext.AddTestAttachment(fileName);
            }
            Session?.Dispose();
            Session = null;
        }

        public void Setup()
        {
            throw new NotImplementedException();
        }

    }
}

